package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	//private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String DB = "test2db";
	private static final String URL = "jdbc:mysql://localhost:3306/" + DB + "?allowMultiQueries=true";
	private static final String USER = "root";
	private static final String PASSWORD = "20062004tVr@";

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		try {
			drop();
		} catch (IOException | DBException e) {
			e.printStackTrace();
		}
	}

	public void drop() throws IOException, DBException {
		List<String> lines = Files.readAllLines(Path.of("sql/db-create.sql"));
		StringBuilder builder = new StringBuilder();
		for (String line : lines) {
			builder.append(line);
		}

		try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
			 Statement stmt = con.createStatement()) {
			stmt.execute(builder.toString());

		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}

	public List<User> findAllUsers() throws DBException {
		String query = "SELECT * FROM users WHERE login LIKE \"user%\"";

		try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
			 Statement statement = con.createStatement();
			 ResultSet result = statement.executeQuery(query)) {

			List<User> users = new ArrayList<>();
			while (result.next()) {
				int id = Integer.parseInt(result.getString(1));
				String login = result.getString(2);

				User user = User.createUser(login);
				user.setId(id);
				users.add(user);
			}
			return users;

		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}

	public void insertUser(User user) throws DBException {
		String query = "INSERT INTO users (login) VALUES (?);\n" +
				"SELECT id FROM users WHERE login=?;";

		try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setString(1, user.getLogin());
			stmt.setString(2, user.getLogin());

			stmt.executeUpdate();

			stmt.getMoreResults();
			ResultSet result = stmt.getResultSet();
			result.next();
			user.setId(result.getInt(1));

			stmt.close();

		} catch (SQLException e) {
			//throw new DBException("", e);
		}
	}

	public void deleteUsers(User... users) throws DBException {
		StringBuilder queryBuilder = new StringBuilder("START TRANSACTION;");
		for (User user : users) {
			requireUserPresent(user);
			queryBuilder.append("DELETE FROM users WHERE id = ?;");
		}
		queryBuilder.append("COMMIT;");

		try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
			PreparedStatement stmt = con.prepareStatement(queryBuilder.toString());
			for (int i = 0; i < users.length; i++) {
				stmt.setInt(i + 1, users[i].getId());
			}
			stmt.executeUpdate();
			stmt.close();

		} catch (SQLException e) {
			rollback();
			throw new DBException("", e);
		}
	}

	public User getUser(String login) throws DBException {
		String query = "SELECT id FROM users WHERE login=?;";

		try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setString(1, login);
			ResultSet result = stmt.executeQuery();

			User user = null;
			if (result.next()) {
				user = User.createUser(login);
				user.setId(result.getInt(1));
			}
			return user;

		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}

	public Team getTeam(String name) throws DBException {
		String query = "SELECT id FROM teams WHERE name=?;";

		try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setString(1, name);
			ResultSet result = stmt.executeQuery();

			Team team = null;
			if (result.next()) {
				team = Team.createTeam(name);
				team.setId(result.getInt(1));
			}
			return team;

		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		String query = "SELECT * FROM teams WHERE name = \"team0\" OR name = \"team1\" OR name = \"team2\" OR name = \"team3\" OR name = \"team4\" OR name = \"team5\" OR name = \"team6\" OR name = \"team7\" OR name = \"team8\" OR name = \"team9\";";

		try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
			 Statement statement = con.createStatement();
			 ResultSet result = statement.executeQuery(query)) {

			List<Team> teams = new ArrayList<>();
			while (result.next()) {
				int id = Integer.parseInt(result.getString(1));
				String name = result.getString(2);

				Team team = Team.createTeam(name);
				team.setId(id);
				teams.add(team);
			}
			return teams;

		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}

	public void insertTeam(Team team) throws DBException {
		String query = "INSERT INTO teams (name) VALUES (?);\n" +
				"SELECT id FROM teams WHERE name=?;";

		try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setString(1, team.getName());
			stmt.setString(2, team.getName());

			stmt.executeUpdate();

			stmt.getMoreResults();
			ResultSet result = stmt.getResultSet();
			result.next();
			team.setId(result.getInt(1));

			stmt.close();

		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}

	public void setTeamsForUser(User user, Team... teams) throws DBException {
		requireUserPresent(user);
		for (Team team : teams) {
			requireTeamPresent(team);
		}

		StringBuilder queryBuilder = new StringBuilder("START TRANSACTION;");
		for (Team ignored : teams) {
			queryBuilder.append("INSERT INTO users_teams (user_id, team_id) VALUES (?, ?);");
		}
		queryBuilder.append("COMMIT;");

		try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
			PreparedStatement stmt = con.prepareStatement(queryBuilder.toString());
			for (int i = 0; i < teams.length; i++) {
				stmt.setInt(i * 2 + 1, user.getId());
				stmt.setInt(i * 2 + 2, teams[i].getId());
			}
			stmt.executeUpdate();
			stmt.close();

		} catch (SQLException e) {
			rollback();
			throw new DBException("", e);
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		String query = "SELECT id, name\n" +
				"FROM (\n" +
				"\tSELECT login, team_id\n" +
				"\tFROM users RIGHT JOIN users_teams\n" +
				"\tON id = user_id\n" +
				") AS tmp\n" +
				"LEFT JOIN teams\n" +
				"ON id = team_id\n" +
				"WHERE login = ?;";

		try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setString(1, user.getLogin());

			ResultSet result = stmt.executeQuery();
			List<Team> teams = new ArrayList<>();

			while (result.next()) {
				int id = result.getInt(1);
				String name = result.getString(2);

				Team team = Team.createTeam(name);
				team.setId(id);
				teams.add(team);
			}

			return teams;

		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}

	public void deleteTeam(Team team) throws DBException {
		requireTeamPresent(team);
		String query = "DELETE FROM teams WHERE id = ?;";

		try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, team.getId());
			stmt.executeUpdate();
			stmt.close();

		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}

	public void updateTeam(Team team) throws DBException {
		// !require that team id present in table

		String query = "UPDATE teams SET name = ? WHERE id = ?;";
		try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setString(1, team.getName());
			stmt.setInt(2, team.getId());
			stmt.executeUpdate();
			stmt.close();

		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}

	// my methods

	private void requireUserPresent(User user) throws DBException {
		if (!isUserPresent(user))
			throw new IllegalArgumentException("User '" + user + "' is not present in database");
	}

	private boolean isUserPresent(User user) throws DBException {
		String query = "SELECT id FROM users WHERE login=?;";

		try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setString(1, user.getLogin());

			ResultSet result = stmt.executeQuery();
			boolean isPresent = result.next();

			if (isPresent) {
				user.setId(result.getInt(1));
			}

			result.close();
			stmt.close();
			return isPresent;

		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}

	private void requireTeamPresent(Team team) throws DBException {
		if (!isTeamPresent(team))
			throw new IllegalArgumentException("Team '" + team + "' is not present in database");
	}

	private boolean isTeamPresent(Team team) throws DBException {
		String query = "SELECT id FROM teams WHERE name=?;";

		try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setString(1, team.getName());

			ResultSet result = stmt.executeQuery();
			boolean isPresent = result.next();

			if (isPresent) {
				team.setId(result.getInt(1));
			}

			result.close();
			stmt.close();
			return isPresent;

		} catch (SQLException e) {
			throw new DBException("", e);
		}
	}

	private void rollback() {
		try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
			Statement stmt = con.createStatement();
			stmt.execute("ROLLBACK;");

		} catch (SQLException e) {
			rollback();
		}
	}

}
