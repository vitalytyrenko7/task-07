DROP database IF EXISTS test2db;

CREATE database test2db;

USE test2db;

CREATE TABLE users (
	id INT PRIMARY KEY auto_increment,
	login VARCHAR(10) UNIQUE
);

CREATE TABLE teams (
	id INT PRIMARY KEY auto_increment,
	name VARCHAR(10)
);

CREATE TABLE users_teams (
	user_id INT NOT NULL,
	team_id INT NOT NULL,
	CONSTRAINT FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
	CONSTRAINT FOREIGN KEY (team_id) REFERENCES teams(id) ON DELETE CASCADE,
	UNIQUE (user_id, team_id)
);

INSERT INTO users VALUES (DEFAULT, 'ivanov');
INSERT INTO teams VALUES (DEFAULT, 'teamA');

SELECT * FROM users;
SELECT * FROM teams;
SELECT * FROM users_teams;

